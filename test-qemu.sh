#!/bin/sh

set -ue

if [ -z "$1" ]; then
    echo "You should pass the archlinux iso path."
    exit -1
fi

ARCHISO_PATH="$1"

function run_verbose {
    echo ""
    echo "Running: $@"
    echo ""
    $@
}

cd "$(dirname $0)"/test_environment

echo "Creating the hard-disk"
qemu-img create ./harddisk.img 20G

echo "Booting the system"
UEFI_PATH="/usr/share/qemu/ovmf-x86_64.bin"
if [ -f "$UEFI_PATH" ]; then
    BOOT_PARAM="-bios $UEFI_PATH"
else
    BOOT_PARAM="-boot d"
fi

mkdir -p config_files
cp ../*.json config_files/

run_verbose qemu-system-x86_64 $BOOT_PARAM -m 4096 -cdrom "$ARCHISO_PATH" -drive file=./harddisk.img,format=raw -drive file=fat:rw:./config_files -display gtk -smp $(nproc)